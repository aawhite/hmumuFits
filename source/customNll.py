from __future__ import division
import sys, copy
sys.path.append("/home/prime/hmumu/ana-200218/code/")
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
sys.path = ["/home/prime/dilepton/pyStap0l8ate/source"] + sys.path
import histograms
import cPickle as pickle
import glob,os,time, random, math, time
import numpy as np
import numpy.ma as ma
# plotting
import matplotlib.pyplot as plt
import roowrap as rw
sys.argv.append( '-b' )
from ROOT import TH1F, RooWorkspace, RooDataHist, RooArgList, TCanvas, RooFit, TText, TPad
from ROOT import TLine, gPad, RooArgSet, RooAddPdf,RooCategory,RooDataSet,RooCmdArg,RooSimultaneous
from ROOT import RooLinkedList, RooAbsReal, TFile, RooExtendPdf
from ROOT.RooFit import *
from ROOT import kGreen, kRed, kFALSE, kBlue
from ROOT import RooAbsPdf, RooRealVar
from ROOT import Math
import ROOT
from math import log, e

def green(*string):
    """ return string as green """
    string = [str(s) for s in string]
    ret = "\033[ 3 2 m{0}\033[ 3 9 m".format(" ".join(string))
    return ret

def yellow(*string):
    """ return string as red """
    string = [str(s) for s in string]
    ret = "\033[ 3 3 m{0}\033[ 3 9 m".format(" ".join(string))
    return ret

# custom NLL class

class nll:
    def __init__(self,w,rdsName,pdfName,chans=None,normRange=None):
        """ Custom NLL fit """
        self.w = w
        self.rdsName = rdsName
        self.normRange = normRange
        self.pdfName = pdfName
        self.chans=chans
        self.rdsByChan = {}
        self.pdfByChan = {}
        # setup
        self.setIntegratorPrecision()
        self.splitByChan()
        self.params={}
        self.getFittableParams()
        self.nSteps=50
        self.res=50

    def setIntegratorPrecision(self):
        """ set integratorPrecision """
        integratorPrecision=1e-8
        RooAbsPdf.defaultIntegratorConfig().setEpsRel(integratorPrecision)
        RooAbsPdf.defaultIntegratorConfig().setEpsAbs(integratorPrecision)
        Math.MinimizerOptions.SetDefaultPrecision(integratorPrecision)


    def exists(self,name):
        """ Check if object exists """
        if self.w.obj(name): return 1
        return 0

    def get(self,name):
        """ get object from workspace """
        if not self.exists(name):
            raise BaseException("Tried to get {0} which doesn't exist".format(name))
        return self.w.obj(name)

    def splitByChan(self):
        """ split up pdf and rds by channels """
        # split up rds
        rds = self.get(self.rdsName)
        x = self.get("x")
        for chan in self.chans:
            cutName = chan+"_rds"
            cutRds = RooDataSet(cutName,cutName,
                        RooArgSet(x),
                        RooFit.Import(rds),
                        RooFit.Cut("channelName==channelName::{0}".format(chan)),
            )
            self.rdsByChan[chan]=cutRds
        # split up pdf
        self.updatePdfs()

    def updatePdfs(self):
        pdf = self.get(self.pdfName)
        for chan in self.chans:
            cutPdf = pdf.getPdf(chan)
            self.pdfByChan[chan]=cutPdf

    def evalChan(self,chan):
        """ evaluate NLL for RooDataSet for this chan
            returns NLL
        """
        pdf = self.pdfByChan[chan]
        rds = self.rdsByChan[chan]
        x = self.get("x")
        localNll = 0
        normalization = pdf.createIntegral(RooArgSet(x),self.normRange).getVal()
        # normalization = 1
        # loop over entries in RooDataSet
        total = 0
        for i in range(rds.numEntries()):
            entryVal = rds.get(i).getRealValue("x")
            total+=entryVal
            x.setVal(entryVal)
            pdfVal = pdf.getVal()/normalization
            if pdfVal<=0: continue
            localNll += log(pdfVal,math.e)
            # print i,entryVal,pdfVal
        print yellow("NLL contribution for {0} is {1}, evaluated for {2} entries with normalization ={3} total sum={4}".\
                     format(chan,localNll,rds.numEntries(),normalization,total))
        return localNll

    # def evalChan(self,chan):
    #     """ evaluate NLL for RooDataSet for this chan
    #         uses rooAbsPdf::extendedTerm
    #         returns NLL
    #     """
    #     pdf = self.pdfByChan[chan]
    #     rds = self.rdsByChan[chan]
    #     rdh = rds.binnedClone()
    #     x = self.get("x")
    #     localNll = 0
    #     # normalization = pdf.createIntegral(RooArgSet(x),self.normRange).getVal()
    #     normalization = 1
    #     # loop over entries in RooDataSet
    #     total = 0
    #     for i in range(rdh.numEntries()):
    #         entryVal = rdh.get(i).getRealValue("x")
    #         total+=entryVal
    #         x.setVal(entryVal)
    #         pdfVal = pdf.extendedTerm(1)/normalization
    #         if pdfVal<=0: continue
    #         localNll += log(pdfVal,math.e)
    #         # print i,entryVal,pdfVal
    #     print yellow("NLL contribution for {0} is {1}, evaluated for {2} entries with normalization ={3} total sum={4}".\
    #                  format(chan,localNll,rdh.numEntries(),normalization,total))
    #     return localNll

    def getVal(self):
        """ evaluate NLL """
        self.updatePdfs() # may not be necesscary
        self.nllSum = 0
        for chan in self.chans:
            # evaluate NLL for channle
            self.nllSum+=self.evalChan(chan)
        print "Global NLL:",self.nllSum
        return -self.nllSum

    def getFittableParams(self):
        """ Find fittable params, modify self.params """
        # get list of fittable parameters in pdf
        rooParams = self.get(self.pdfName).getParameters(self.get(self.rdsName))
        iterator = rooParams.fwdIterator()
        while True:
            var=iterator.next()
            if not var: break
            # skip variables with no range
            if var.isConstant(): continue
            if var.getMin()==var.getMax(): continue
            if var.getMin()==-1e30 or var.getMax()==1e30: continue
            # add variable and range to params dict
            self.params[var.GetName()]={"var":var}
            self.params[var.GetName()]["trueMin"]=var.getMin() # actual limit
            self.params[var.GetName()]["trueMax"]=var.getMax()
            self.params[var.GetName()]["min"]=var.getMin() # limit used for fit
            self.params[var.GetName()]["max"]=var.getMax()
        print "PDF has following fittable params"
        print self.params.keys()
        print self.params


    def fit(self,verbose=False):
        """ main driving function for fitting """
        params = self.params
        curChi2 = self.getVal()
        if verbose: print "#"*100
        if verbose: print green("Initial Chi2=",curChi2)
        if verbose: print "#"*100
        for stepI in range(self.nSteps):
            if verbose: print "#"*50
            if verbose: print green("Step number", stepI)
            if verbose: print "#"*50
            for varName in params.keys():
                # There are three cases for a choice of steps
                # 1) The upper step is better
                # 2) The lower step is better
                # 3) The current step is a minimum
                var = self.params[varName]["var"]
                rangeWidth = params[varName]["max"]-params[varName]["min"]
                step = (rangeWidth)/self.res
                origVal = var.getVal()
                ##########
                # 1) Try up
                ##########
                var.setVal(origVal+step)
                newChi2 = self.getVal()
                if newChi2<curChi2: # move to this new location
                    if verbose: print green("Moving variable",varName,"up to new value",var.getVal(), "with chi2=",newChi2, "improvement=", curChi2-newChi2)
                    curChi2=newChi2
                    continue
                ##########
                # 2) Try down
                ##########
                var.setVal(origVal-step)
                newChi2 = self.getVal()
                if newChi2<curChi2: # move to this new location
                    if verbose: print green("Moving variable",varName,"down to new value",var.getVal(), "with chi2=",newChi2, "improvement=", curChi2-newChi2)
                    curChi2=newChi2
                    continue
                ##########
                # 3) At local min
                ##########
                # if at minimum, reduce upper and lower bounds
                var.setVal(origVal) # return to original value
                newCenter = var.getVal()
                newRange = rangeWidth/2
                newMax = newCenter + newRange/2
                newMin = newCenter - newRange/2
                # avoid going outside range
                newMax = min(newMax,params[varName]["trueMax"])
                newMin = max(newMin,params[varName]["trueMin"])
                # update range
                params[varName]["max"] = newMax
                params[varName]["min"] = newMin
                if verbose: print green("*** Found minimum chi2=",newChi2,", adjusting range of ",varName, "to", newMin, newMax)




