from __future__ import division
import sys, copy
sys.path.append("/home/prime/hmumu/ana-200218/code/")
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
sys.path = ["/home/prime/dilepton/pyStap0l8ate/source"] + sys.path
import histograms
import cPickle as pickle
import glob,os,time, random, math, time
import numpy as np
import numpy.ma as ma
# plotting
import matplotlib.pyplot as plt
import roowrap as rw
sys.argv.append( '-b' )
from ROOT import TH1F, RooWorkspace, RooDataHist, RooArgList, TCanvas, RooFit, TText, TPad
from ROOT import TLine, gPad, RooArgSet, RooAddPdf,RooCategory,RooDataSet,RooCmdArg,RooSimultaneous
from ROOT import RooLinkedList, RooAbsReal, TFile, RooExtendPdf
from ROOT.RooFit import *
from ROOT import kGreen, kRed, kFALSE, kBlue
from ROOT import RooAbsPdf, RooRealVar
from ROOT import Math
import ROOT

def firstUpper(s):
    return s[0].upper()+s[1:]

def yellow(*string):
    """ return string as red """
    string = [str(s) for s in string]
    ret = "\033[ 3 3 m{0}\033[ 3 9 m".format(" ".join(string))
    return ret

def red(*string):
    """ return string as red """
    string = [str(s) for s in string]
    ret = "\033[ 3 1 m{0}\033[ 3 9 m".format(" ".join(string))
    return ret

def green(*string):
    """ return string as green """
    string = [str(s) for s in string]
    ret = "\033[ 3 2 m{0}\033[ 3 9 m".format(" ".join(string))
    return ret

def removeNan(x,y):
    x = list(x)
    y = list(y)
    for i in reversed(range(len(x))):
        if np.isnan(y[i]):
            x.pop(i)
            y.pop(i)
    return np.array(x), np.array(y)



################################################################################
# Class for performing simultaneous fits
# Takes input of dict of histograms ordered:
#   hists[channel][sigBkg] = TH1F
# How data is organized
#   * At all times, the "true" version of the data is stored in self.w workspace
#   * The "true" version of the data is the RDS version, not the RDH version
################################################################################

class hmmFit:
    def __init__(self,hists):
        # load internal structures
        self._hists = hists
        self._chans = hists.keys()
        self._nBins = None # number of bins, taken from first hist loaded
        print "hmmFit launched for channles", self._chans
        # set up workspace
        self._initWksp()
        # process hists (create RDS, get yields)
        self._processHists()
        # set up signal and background models, do channel fits
        self._setupModels()
        # make combined sets for simultaneous fits
        self._makeCombined()
        # do simultaneous fit
        self._doSimFit()

################################################################################
# Tools for class
################################################################################

    def exists(self,name):
        """ Check if object exists """
        if self.w.obj(name): return 1
        return 0

    def get(self,name):
        if not self.exists(name):
            raise BaseException("Tried to get {0} which doesn't exist".format(name))
        return self.w.obj(name)

    def add(self,obj):
        """ Add to self.w workspace """
        print "#"*50
        print "Adding object", obj
        getattr(self.w,"import")(obj)
        print "#"*50

    def addStr(self,string):
        """ Add string via factory """
        print "#"*50
        print green("Adding string", string)
        getattr(self.w,"factory")(string)
        print "#"*50

    def addFloat(self,name,value):
        """ Add number with value to workspace with name """
        v = RooRealVar(name,name,float(value))
        self.add(v)

    def _fit(self,pdfName,dataName,fitRange):
        # fit wrapper
        print "#"*10
        print green("Fitting",pdfName, dataName)
        print "#"*10
        pdf = self.get(pdfName)
        data =self.get(dataName)
        result=self.get(pdfName).fitTo(self.get(dataName),RooFit.Range(fitRange),RooFit.PrintLevel(-1),RooFit.Save());

    def _histToRdh(self,hist):
        print ""
        """ Convert histogram to RooDataHist, add to workspace, return name """
        name= hist.GetName()+"_rdh"
        x   = self.get("x")
        print green("Loading RDH", name)
        rdh = RooDataHist(name,name,RooArgList(x),hist)
        self.add(rdh)
        return name

    def _histToRds(self,hist):
        """ Convert histogram to RooDataSet, add to workspace, return name """
        x = self.get("x")
        name = hist.GetName()+"_rds"
        print green("Loading RDS", name)
        rds = RooDataSet(name,name,RooArgSet(x))
        # loop over bins in th1f hist
        for i in range(1,hist.GetNbinsX()+1):
            histValue =hist.GetBinContent(i)
            histCenter=hist.GetBinCenter(i)
            x.setVal(histCenter)
            if histCenter<self.fullMin: continue
            if histCenter>self.fullMax: continue
            for j in range(int(histValue)):
                rds.add(RooArgSet(x))
            # for j in range(int(histValue/1000)): rds.add(RooArgSet(x))
            # rds.add(RooArgSet(x),int(histValue/100))
            # rds.add(RooArgSet(x),int(histValue))
        self.add(rds)
        return name

################################################################################
# Plotting
################################################################################

    def _nllPlot(self):
        """ Make matplotlib style plot using the rooWrap class for NLL"""
        print "#"*100
        print "*"*20,"plotting","*"*20
        # get things to plot from workspace
        plt.cla(); plt.clf()
        mu  = self.get("mu")
        mu.setBins(int(1e3))
        nllRaw = self.get("nll")
        nll = rw.rooWrap(nllRaw,mu)
        nll.x, nll.y=removeNan(nll.x,nll.y)
        nll.y = nll.y - min(nll.y)
        # go to -2 log(l)
        nll.y*=2
        # find low, high limits for plot
        ymax = 20
        lowi = np.argmin(nll.y[1:-1])
        while lowi>1 and nll.y[lowi]<ymax: lowi-=1
        highi = np.argmin(nll.y[1:-1])
        while highi<len(nll.y)-3 and nll.y[highi]<ymax: highi+=1
        # print "nll.y: ", nll.y
        # print "nll.x: ", nll.x
        print "PLOT bin choices:", lowi, highi, np.argmin(nll.y)
        if lowi==highi:
            print nll.y
            print lowi, highi, np.argmin(nll.y)
            raw_input("Issue with finding nll min/max")
        lowi-=1; highi+=1
        # rebin mu, remake nll
        mu.setMin(nll.x[lowi])
        mu.setMax(nll.x[highi])
        nll = rw.rooWrap(nllRaw,mu)
        nll.x, nll.y=removeNan(nll.x,nll.y)
        # subtract minimum
        nll.y = nll.y - min(nll.y)
        # find low, high limits for plot
        minimumI = np.argmin(nll.y[1:-1])
        ymax = 2
        lowi = minimumI
        while lowi>1 and nll.y[lowi]<ymax: lowi-=1
        highi = minimumI
        while highi<len(nll.y)-2 and nll.y[highi]<ymax: highi+=1
        # find +/- 1 for NLL
        upSigma = minimumI
        while upSigma<len(nll.y)-2 and nll.y[upSigma]<0.5: upSigma+=1
        downSigma = minimumI
        while downSigma>1 and nll.y[downSigma]<0.5: downSigma-=1
        # print "nll.y: ", nll.y
        # print "nll.x: ", nll.x
        print "PLOT bin choices:", lowi, highi, minimumI
        if lowi==highi:
            print lowi, highi, minimumI
            raw_input("Issue with finding nll min/max")
        lowi-=1; highi+=1
        ######################
        # make matplotlib plot
        ######################
        plt.figure(figsize=(5,5))
        # plt.ylim([0,ymax])
        # ticksInside(plt)
        plt.plot(nll.x[lowi:highi],nll.y[lowi:highi])
        a = nll.x[minimumI]
        b = abs(a-nll.x[upSigma])
        c = abs(a-nll.x[downSigma])
        plt.plot([],[]," ",label=r"$\mu_={0:.2f}^{{+{1:.2f}}}_{{-{2:.2f}}}$".format(a,b,c))
        plt.xlabel(r"Signal Strength $\mu$")
        plt.ylabel(r"$-2\log{L}$")
        # plt.title("name",fontsize=16)
        path = "plots/nll.png"
        width=plt.axis()[:2]
        plt.plot(width,[1.0]*2,"k",alpha=0.5)
        plt.plot(width,[2.0]*2,"k",alpha=0.5)
        plt.legend(fontsize=14,loc=1, frameon=False)
        # plt.savefig(path)
        plt.savefig(path,bbox_inches="tight")


    def _simplePlot(self,name,pdfName,dataName):
        """ Make simple comparison plot between pdf and data """
        plt.clf(); plt.cla()
        x = self.get("x")
        dat = rw.rooWrap(self.get(dataName),x)
        pdf = rw.rooWrap(self.get(pdfName),x)
        pdf.cut(minRange=self.fullMin,maxRange=self.fullMax)
        dat.cut(minRange=self.fullMin,maxRange=self.fullMax)
        # normalize
        pdf.normalizeTo(dat,minRange=self.fullMin,maxRange=self.fullMax)
        # make plot
        plt.plot(pdf.x,pdf.y,"b-",label="PDF")
        plt.plot(dat.x,dat.y,"ok",label="Data")
        plt.xlabel(r"$M_{\mu\mu}$ GeV")
        plt.title(name)
        plt.savefig("plots/{0}.png".format(name),bbox_inches='tight')
        plt.close()

    def _dataSlicePlot(self,chan,title):
        """ Make a plot based on chan from simPdf, simRds 
            This plot should correspond to EXACTLY what is used for the
            simultaneous fit
        """
        plt.cla(); plt.clf()
        # simultaneous objects
        simPdf = self.get("simPdf").getPdf(chan)
        # simRdh = self.get("simRdh")
        simRds = self.get("simRds")
        x = self.get("x")
        # make cut version of simRdh
        cutName = "sliced_"+chan+"_rds"
        cutRds = RooDataSet(cutName,cutName,
                            RooArgSet(x),
                            RooFit.Import(simRds),
                            RooFit.Cut("channelName==channelName::{0}".format(chan)),
                            )
        cutName = "sliced_"+chan+"_rdh"
        cutRdh = cutRds.binnedClone(cutName,cutName)
        # cutRdh = self.get("simRdh")
        # make plotables
        dat = rw.rooWrap(cutRdh,x)
        pdf = rw.rooWrap(simPdf,x)
        pdf.cut(minRange=self.fullMin,maxRange=self.fullMax)
        dat.cut(minRange=self.fullMin,maxRange=self.fullMax)
        # normalize
        pdf.normalizeTo(dat,minRange=self.fullMin,maxRange=self.fullMax)
        # make plot
        plt.plot(pdf.x,pdf.y,"b-",label="PDF")
        plt.plot(dat.x,dat.y,"ok",label="Data")
        plt.xlabel(r"$M_{\mu\mu}$ GeV")
        plt.title("{0}-{1}".format(chan,title))
        plt.savefig("plots/slice-{0}-{1}.png".format(chan,title),bbox_inches='tight')
        plt.close()

    def _fixVariables(self,fixed=True):
        """ Fix all variables except x, mu """
        print green("Fixing variables: {0}".format(fixed))
        allVars=self.w.allVars() # this seperate step is needed
        iterate=allVars.createIterator()
        var=iterate.Next()
        while var!=None:
            if var.GetName() not in ["x","mu"]:
                print yellow("Fixing", var.GetName())
                var.setConstant(fixed)
            else:
                print yellow("NOT Fixing", var.GetName())
            var=iterate.Next()

################################################################################
# Procedures
################################################################################

    def _processHists(self):
        """ Process histograms in _hists
            Create RooDataSets, RooDataHists in dictionaires
        """
        # create RooDataSets and RooDataHists
        # these objects store the *names* of the created roo-objs
        self._datasets  = {} # RooDataSets
        self._datahists = {} # RooDataHists
        for chan in self._chans:
            sig = self._hists[chan]["sig"]
            dat = self._hists[chan]["dat"]
            bkg = self._hists[chan]["bkg"]
            self._datasets[chan]         = {}
            self._datasets[chan]["sig"]  = self._histToRds(sig)
            self._datasets[chan]["dat"]  = self._histToRds(dat)
            self._datasets[chan]["bkg"]  = self._histToRds(bkg)
            self._datahists[chan]        = {}
            self._datahists[chan]["sig"] = self._histToRdh(sig)
            self._datahists[chan]["dat"] = self._histToRdh(dat)
            self._datahists[chan]["bkg"] = self._histToRdh(bkg)
        # get yields based on RooDataHists
        # also, add them to workspace
        x = self.get("x")
        for chan in self._chans:
            # get histos
            sig = rw.rooWrap(self.get(self._datahists[chan]["sig"]),x)
            dat = rw.rooWrap(self.get(self._datahists[chan]["dat"]),x)
            bkg = rw.rooWrap(self.get(self._datahists[chan]["bkg"]),x)
            # get yields
            nSig = sig.sum(minRange=self.fullMin,maxRange=self.fullMax)
            nDat = dat.sum(minRange=self.fullMin,maxRange=self.fullMax)
            nBkg = bkg.sum(minRange=self.fullMin,maxRange=self.fullMax)
            # save values
            self.addFloat(chan+"_nBkg",nBkg)
            self.addFloat(chan+"_nSig",nSig)
            self.addFloat(chan+"_nDat",nDat)


    def _initWksp(self):
        """ Init workspace 
            Defines self.w
        """
        print green("initWksp")
        integratorPrecision=1e-8
        RooAbsPdf.defaultIntegratorConfig().setEpsRel(integratorPrecision)
        RooAbsPdf.defaultIntegratorConfig().setEpsAbs(integratorPrecision)
        Math.MinimizerOptions.SetDefaultPrecision(integratorPrecision) 
        # create workspace
        self.w = RooWorkspace("w","w")
        # create variable
        self.srLow=120
        self.srHigh=130
        self.fullMin=110
        self.fullMax=160
        self.addStr("fullMin[{0}]".format(self.fullMin))
        self.addStr("fullMax[{0}]".format(self.fullMax))
        self.addStr("srLow[{0}]".format(self.srLow))
        self.addStr("srHigh[{0}]".format(self.srHigh))
        self.addStr("mu[1,-100,100]")
        self.addStr("x[{0},{0},{1}]".format(self.fullMin,self.fullMax))
        # create range
        x = self.get("x")
        x.setBins(100)
        x.setRange("fullRange",self.fullMin,self.fullMax)
        x.setRange("srRange",self.srLow,self.srHigh)
        print green("done initWksp")

    def _setupModels(self):
        """ Set up signal and background models for all channels """
        x = self.get("x")
        # simple model
        for chan in self._chans:
            # names
            bkgPdfName="bkgPdf{0}".format(firstUpper(chan))
            sigPdfName="sigPdf{0}".format(firstUpper(chan))
            addPdfName="addPdf{0}".format(firstUpper(chan))
            sigCoefName = "nSigCoef{0}".format(firstUpper(chan))
            # signal coefficient
            self.addStr("prod:{0}(mu,{1})".format(sigCoefName,chan+"_nSig"))
            #########################################################
            # background model
            self.addStr("RooVoigtian::BW_bg{0} (x, mBW{0}[91.2], ZWidth{0}[2.49], sigma{0}[2])".format(firstUpper(chan))) 
            self.addStr("EXPR::exp_bg{0}('exp(a2_bg{0}*(x/1))*(1./pow((x/1), a3_bg{0}))',   x, a2_bg{0}[0,-1,1],a3_bg{0}[2,0,25])".format(firstUpper(chan)))
            self.addStr("SUM::{0}Pdf( BW_bg{1} , frac_bg{1}[0.1,0,1] * exp_bg{1} )".format(bkgPdfName,firstUpper(chan))) 
            self.get(bkgPdfName+"Pdf").fixAddCoefNormalization(RooArgSet(x),True)
            # extend and fix range
            bkgPdf = RooExtendPdf(bkgPdfName,bkgPdfName,self.get(bkgPdfName+"Pdf"),self.get(chan+"_nDat"),"fullRange")
            # bkgPdf.fixAddCoefNormalization(RooArgSet(x),True)
            # bkgPdf.fixAddCoefRange("fullRange",True)
            print "Adding background", bkgPdf
            bkgPdf.Print()
            self.add(bkgPdf)
            #########################################################
            # signal model
            self.addStr("BW_mean_sig{0}[125,121,129]".format(firstUpper(chan)))
            self.addStr("BW_mean_sig_gaus{0}[125,123,127]".format(firstUpper(chan))) 
            self.addStr("RooCBShape::sigCB{0}(x, BW_mean_sig{0}, CB_sigma_sig{0}[2,1,4], CB_a_sig{0}[1.75,1,2], CB_n_sig{0}[1.])".format(firstUpper(chan)))
            self.addStr("RooGaussian::sigGF{0}(x, BW_mean_sig_gaus{0}, BW_sigma_sig{0}[5,1,6])".format(firstUpper(chan)))
            if False: # don't extend signal
                self.addStr("SUM:{0}(sigCB{1}, frac_sig{1}[0.3,0, 0.4]* sigGF{1})".format(sigPdfName,firstUpper(chan)))
                self.get(sigPdfName).fixAddCoefNormalization(RooArgSet(x),True)
                sigPdf = self.get(sigPdfName)
            else: # Extend and fix range
                self.addStr("SUM:{0}Pdf(sigCB{1}, frac_sig{1}[0.3,0, 0.4]* sigGF{1})".format(sigPdfName,firstUpper(chan)))
                self.get(sigPdfName+"Pdf").fixAddCoefNormalization(RooArgSet(x),True)
                sigPdf = RooExtendPdf(sigPdfName,sigPdfName,self.get(sigPdfName+"Pdf"),self.get(chan+"_nSig"),"fullRange")
            # sigPdf.fixAddCoefNormalization(RooArgSet(x),True)
            # sigPdf.fixAddCoefRange("fullRange",True)
            print "Adding signal", sigPdf
            sigPdf.Print()
            self.add(sigPdf)
            #########################################################
            # combined model
            addPdf = RooAddPdf( addPdfName,addPdfName,
                                RooArgList( self.get(bkgPdfName),self.get(sigPdfName)),
                                RooArgList( self.get(chan+"_nDat"),self.get(sigCoefName))
                              )
            self.add(addPdf)
            #########################################################
            # perform fit
            bkgName = "dat_"+chan+"_rdh" # fit to data
            sigName = "sig_"+chan+"_rdh" # fit to sig MC
            self._fit(bkgPdfName,bkgName,"fullRange")
            self._fit(sigPdfName,sigName,"fullRange")
            self._simplePlot("sigFit-{0}".format(chan),sigPdfName,sigName)
            self._simplePlot("bkgFit-{0}".format(chan),bkgPdfName,bkgName)

    def _makeCombined(self):
        """ Make combined dataset """
        print green("Making combined datasets")
        x = self.get("x")
        #############################################################
        # make RooCategory
        categories = RooCategory("channelName","channelName")
        [categories.defineType(chan) for chan in self._chans]
        self.add(categories)
        #############################################################
        # make dataset
        # follow instructions: https://root-forum.cern.ch/t/combining-roodatasets-using-std-map-in-pyroot/16471/19
        dataMap = ROOT.std.map('string, RooDataSet*')()
        dataMap.keepalive = list()
        for chan in self._chans:
            print green("Adding channel",chan)
            rds = self.get("dat_"+chan+"_rds")
            dataMap.keepalive.append(rds)
            pair=ROOT.std.pair("const string, RooDataSet*")(chan, rds)
            dataMap.insert(dataMap.cbegin(), pair)
            # dataMap.insert(dataMap.cend(), pair)
        simRds = RooDataSet("simRds","simRds",RooArgSet(x),RooFit.Index(categories),RooFit.Import(dataMap))
        simRdh = simRds.binnedClone("simRdh","simRdh")
        self.add(simRds)
        self.add(simRdh)
        #############################################################
        # make simultaneous pdf
        simPdf = RooSimultaneous("simPdf","simPdf",categories)
        for chan in self._chans:
            addPdfName="addPdf{0}".format(firstUpper(chan))
            simPdf.addPdf(self.get(addPdfName),chan)
        simPdf.fixAddCoefNormalization(RooArgSet(x),True)
        # simPdf.fixAddCoefRange("fullRange",True) # TODO: this doesn't seem to change much, but I think it should be included
        self.add(simPdf)
        #############################################################
        # make plots for checking combined data set
        for chan in self._chans:
            self._dataSlicePlot(chan,"prefit")

    def _doSimFit(self):
        """ Run the simultaneous NLL fit """
        print green("Doing simultaneous fit")
        #############################################################
        # fix all variables not used in sim-fit
        x = self.get("x")
        self._fixVariables(fixed=True)
        # do nll fit
        print red("*** Creating nll ***")
        nll = self.get("simPdf").createNLL(self.get("simRdh"),RooFit.NumCPU(4))
        # nll = self.get("simPdf").createNLL(self.get("simRds"),RooFit.NumCPU(4))
        # data = self.get("dat_nonCentralHighPt_rdh")
        # pdf = self.get("addPdfNonCentralHighPt")
        # nll = pdf.createNLL(data,RooFit.NumCPU(4))
        # nll = self.get("simPdf").createChi2(self.get("simRdh"),RooFit.NumCPU(4))
        nll.SetTitle("nll"); nll.SetName("nll")
        print red("*** Done creating nll ***")
        self.add(nll)
        ROOT.RooMinuit(nll).migrad()
        ROOT.RooMinuit(nll).minos()
        # best mu value
        nllMinFit=nll.getVal()
        self.addFloat("bestNll",nllMinFit)
        muMinFit=self.get("mu").getVal()
        self.addFloat("bestMu",muMinFit)
        print green("Best mu fit:",muMinFit)
        # make plot for mu
        # self._nllPlot()
        # make plots for checking
        for chan in self._chans:
            self._dataSlicePlot(chan,"postfit")

    def test(self):
        """ run a test """
        print red("#"*10)
        mu = self.get("mu")
        nll = self.get("nll")
        nllMin = self.get("bestNll").getVal()
        muMin=self.get("bestMu").getVal()
        for chan in self._chans:
            sigCoefName = "nSigCoef{0}".format(firstUpper(chan))
            coef = self.get(sigCoefName)
            print green("Running test for chan",chan)
            for i in [muMin,-10,-1,0,1,2]:
                mu.setVal(i)
                print "i={0},\tmu={1},\tcoef={2},\tnll={3}".format(i, mu.getVal(),coef.getVal(), nll.getVal()-nllMin)
        print green("Best mu fit:",muMin)








import time
start=time.time()
import loadMiha
hists = loadMiha.loadMiha()
# hists = {"nonCentralHighPt":hists["nonCentralHighPt"]}
os.popen("rm plots/* workspace.root")

fit = hmmFit(hists)
# fit.test()
stop=time.time()
print green("Run time",round((stop-start)/60,2),"m")

f = TFile.Open("workspace.root","recreate")
fit.w.Write()
f.Close()

print "DONE "*100
