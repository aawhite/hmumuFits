from __future__ import division
import sys, copy
sys.path.append("/home/prime/hmumu/ana-200218/code/")
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
sys.path = ["/home/prime/dilepton/pyStap0l8ate/source"] + sys.path
import histograms
import cPickle as pickle
import glob,os,time, random, math, time
import numpy as np
import numpy.ma as ma
# plotting
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import roowrap as rw
sys.argv.append( '-b' )
from ROOT import TH1F, RooWorkspace, RooDataHist, RooArgList, TCanvas, RooFit, TText, TPad
from ROOT import TLine, gPad, RooArgSet, RooAddPdf,RooCategory,RooDataSet,RooCmdArg,RooSimultaneous
from ROOT import RooLinkedList, RooAbsReal, TFile, RooExtendPdf
from ROOT.RooFit import *
from ROOT.RooFit import NumCPU, ShiftToZero, Range, Save
from ROOT import kGreen, kRed, kFALSE, kBlue
from ROOT import RooAbsPdf, RooRealVar, RooGaussian
from ROOT import Math
import ROOT
from extras import *

# fix seeds
random.seed(1)
np.random.seed(1)

################################################################################
# Class for performing simultaneous fits
# Takes input of dict of histograms ordered:
#   hists[channel][sigBkg] = TH1F
# How data is organized
#   * At all times, the "true" version of the data is stored in self.w workspace
#   * The "true" version of the data is the RDS version, not the RDH version
# The names of the input histogram must follow: channel_sig_dy
################################################################################

class hmmFit:
    def __init__(self,hists,**kwargs):
        # load internal structures
        if "fixParamsInSimFit" in kwargs: self._fixParamsInSimFit=kwargs["fixParamsInSimFit"]
        else: self._fixParamsInSimFit=False
        if "plotOn" in kwargs: self._plotOn=kwargs["plotOn"]
        else: self._plotOn=True
        if "fitOn" in kwargs: self._fitOn=kwargs["fitOn"]
        else: self._fitOn=True
        if "loadFromWorkspace" in kwargs: self._loadFromWorkspace=kwargs["loadFromWorkspace"]
        else: self._loadFromWorkspace=False
        if "blind" in kwargs: self._blind=kwargs["blind"]
        else: self._blind=None
        if "useToy" in kwargs: self._useToy=kwargs["useToy"]
        else: self._useToy=False
        self._hists = hists
        self._chans = hists.keys() # list of channels, ie vbfLoose, centralLowPt
        # TODO: these dictionaries should be replaced with naming methods
        self._sigNames = {chan:[chan+"_sig_"+x for x in hists[chan]["sig"]] for chan in self._chans}
        self._bkgNames = {chan:[chan+"_bkg_"+x for x in hists[chan]["bkg"]] for chan in self._chans}
        self._datNames = {chan:[chan+"_dat_"+x for x in hists[chan]["dat"]] for chan in self._chans}

        self._nBins = None # number of bins, taken from first hist loaded
        self._log = open("plots/log.csv","w")
        self.muVals=[]
        print "hmmFit launched for channles", self._chans
        if not self._loadFromWorkspace:
            # set up workspace
            self._initWksp()
            # process hists (create RDS, get yields)
            self._processHists()
            # set up signal and background models, do channel fits
            self._setupModels()
            # self._makeVariableSets()
            # make combined sets for simultaneous fits
            self._makeCombined()
            # do simultaneous fit
        else:
            f = TFile.Open(self._loadFromWorkspace)
            self.w = f.Get("w") # workspace
        self._doSimFit()
        # do nll scan
        self._doNllScan()
        self._log.close()


################################################################################
# Tools for class
################################################################################

    def _blindArray(self,x,y):
        """ Blind array y based on x, if self._blind defined """
        if self._blind == None: return y
        for i in range(len(x)):
            if x[i]>=self._blind[0] and x[i]<=self._blind[1]:
                y[i] = np.nan
        return y


    def exists(self,name):
        """ Check if object exists """
        if self.w.obj(name): return 1
        return 0

    def set(self,name):
        """ return named set form workspace """
        set = self.w.set(name)
        if not set:
            raise BaseException(red("Tried to get ret {0} which doesn't exist".format(name)))
        return set

    def get(self,name):
        if not self.exists(name):
            raise BaseException(red("Tried to get {0} which doesn't exist".format(name)))
        return self.w.obj(name)

    def add(self,obj):
        """ Add to self.w workspace """
        print "#"*50
        print "Adding object", obj
        result = getattr(self.w,"import")(obj)
        # check that was added properly
        if result==None: 
            raise BaseException(red("Error when adding object via RooWorkspace Factory"))
        # print self.get(obj.GetName())
        print "#"*50

    def addStr(self,string):
        """ Add string via factory """
        print "#"*50
        print green("Adding string", string)
        result = getattr(self.w,"factory")(string)
        # check that was added properly
        if result==None: 
            print red("Tried and failed to factory following string")
            print red(string)
            raise BaseException(red("Error when adding object via RooWorkspace Factory"))
        print "#"*50

    def addFloat(self,name,value):
        """ Add number with value to workspace with name """
        v = RooRealVar(name,name,float(value))
        self.add(v)

    def _fit(self,pdfName,dataName,fitRange):
        # fit wrapper
        print "#"*10
        print green("Fitting",pdfName, dataName)
        print "#"*10
        pdf = self.get(pdfName)
        data =self.get(dataName)
        result=self.get(pdfName).fitTo(self.get(dataName),RooFit.Range(fitRange),RooFit.PrintLevel(-1),RooFit.Save());
        print "RESULT:",result

    def _rdhToy(self,rdh):
        """ return toy based on RDH """
        toyName = rdh.GetName()
        toy = RooDataHist(rdh,toyName)
        for i in range(toy.numEntries()):
            toy.get(i)
            val = toy.weight()
            if val<0: continue # silly negative wieghts
            mod = np.random.poisson(val,1)[0]
            mod = np.random.poisson(mod,1)[0]
            mod = np.random.poisson(mod,1)[0]
            mod = np.random.poisson(mod,1)[0]
            mod = np.random.poisson(mod,1)[0]
            toy.set(mod)
        return toy


    def _histsToRdh(self,histDict):
        print ""
        """ Convert histogram to RooDataHist, add to workspace, return name 
            If self._useToy, throw a toy. ONLY FOR NON-SIG
        """
        x   = self.get("x")
        names=[]
        for mc in histDict.keys():
            hist=histDict[mc]
            name= hist.GetName()+"_rdh"
            print green("Loading RDH", name)
            rdh = RooDataHist(name,name,RooArgList(x),hist)
            # calculate yields
            srYield = rdh.sumEntries("x","srRange")
            fullYield = rdh.sumEntries("x","fullRange")
            self.log("srYield,{0},{1}".format(name,srYield))
            self.log("fullYield,{0},{1}".format(name,fullYield))
            # throw a toy based on input hist
            if self._useToy and "sig" not in name:
                rdh = self._rdhToy(rdh)
            self.add(rdh)
            names.append(name)
        return names

    def _rdhToRds(self,rdhNameList):
        """ Convert RooDataHist to RooDataSet, add to workspace
            rdhNameList is list of RDH names, already in workspace, to copy
            return names
        """
        x = self.get("x")
        names = []
        for rdhName in rdhNameList:
            # base rdh
            rdh=self.get(rdhName)
            name = rdh.GetName().replace("_rdh","")+"_rds"
            # convert into hist for conversion to rds
            hist = rdh.createHistogram("tmpHist",x)
            print green("Loading RDS", name)
            rds = RooDataSet(name,name,RooArgSet(x))
            # loop over bins in th1f hist
            for i in range(1,hist.GetNbinsX()+1):
                histValue =hist.GetBinContent(i)
                histCenter=hist.GetBinCenter(i)
                x.setVal(histCenter)
                if histCenter<self.get("fullMin").getVal(): continue
                if histCenter>self.get("fullMax").getVal(): continue
                # add integer parts
                for j in range(int(histValue)):
                    rds.add(RooArgSet(x))
                # add fractional parts
                res = 100
                if random.randint(0,res) < (histValue%1)*res:
                    rds.add(RooArgSet(x))
                # for j in range(int(histValue/1000)): rds.add(RooArgSet(x))
                # rds.add(RooArgSet(x),int(histValue/100))
                # rds.add(RooArgSet(x),histValue)
            self.add(rds)
            names.append(name)
        return names

    def _histsToRds(self,histDict):
        """ Convert histogram to RooDataSet, add to workspace, return name """
        x = self.get("x")
        names = []
        for mc in histDict.keys():
            hist=histDict[mc]
            name = hist.GetName()+"_rds"
            print green("Loading RDS", name)
            rds = RooDataSet(name,name,RooArgSet(x))
            # loop over bins in th1f hist
            for i in range(1,hist.GetNbinsX()+1):
                histValue =hist.GetBinContent(i)
                histCenter=hist.GetBinCenter(i)
                x.setVal(histCenter)
                if histCenter<self.get("fullMin").getVal(): continue
                if histCenter>self.get("fullMax").getVal(): continue
                for j in range(int(histValue)):
                    rds.add(RooArgSet(x))
                # for j in range(int(histValue/1000)): rds.add(RooArgSet(x))
                # rds.add(RooArgSet(x),int(histValue/100))
                # rds.add(RooArgSet(x),int(histValue))
            self.add(rds)
            names.append(name)
        return names

    def log(self,s):
        """ write string s to _log """
        self._log.write(s+"\n")
        self._log.flush()

################################################################################
# Plotting
################################################################################

    def _nllPlot(self):
        """ """
        frame = self.get("mu").frame(Range(0,5))
        frame.SetMinimum(-1)
        frame.SetMaximum(10)
        frame.GetYaxis().SetRangeUser(0,5)
        self.get("nll").plotOn(frame,ShiftToZero())
        c = TCanvas("c","c",400,400)
        frame.Draw()
        raw_input()
        c.SaveAs("plots/nll.png")


    def _vizPdf(self,pdf):
        """ make tree showing PDF definition """
        name = pdf.GetName()
        pdf.graphVizTree("plots/{0}.dot".format(name))
        os.popen("dot -Tgif -o plots/{0}.gif plots/{0}.dot".format(name))

    def _simplePlot(self,name,pdfName,dataName):
        """ Make simple comparison plot between pdf and data """
        print green("Running simple plot {0}, {1}".format(pdfName,dataName))
        plt.clf(); plt.cla()
        x = self.get("x")
        dat = rw.rooWrap(self.get(dataName),x)
        pdf = rw.rooWrap(self.get(pdfName),x)
        pdf.cut(minRange=self.get("fullMin").getVal(),maxRange=self.get("fullMax").getVal())
        dat.cut(minRange=self.get("fullMin").getVal(),maxRange=self.get("fullMax").getVal())
        # normalize
        pdf.normalizeTo(dat,minRange=self.get("srMin").getVal(),maxRange=self.get("srMax").getVal(),invert=True)
        # make plot
        plt.plot(pdf.x,pdf.y,"b-",label="PDF")
        plt.plot(dat.x,dat.y,"ok",label="Data")
        plt.xlabel(r"$M_{\mu\mu}$ GeV")
        plt.title(name)
        plt.savefig("plots/{0}.png".format(name),bbox_inches='tight')
        plt.close()
        # save log
        self._log.write("data-{0},{1}\n"  .format(name,dat.sum(minRange=self.get("srMin").getVal(),maxRange=self.get("srMax").getVal())))
        self._log.write("bkgPdf-{0},{1}\n".format(name,pdf.sum(minRange=self.get("srMin").getVal(),maxRange=self.get("srMax").getVal())))
        self._log.flush()

    def _dataSlicePlot(self,chan,title,labels=[]):
        print green("Making plot",chan,title)
        plt.cla(); plt.clf()
        # make the plot
        plt.figure(figsize=(5,5))
        # simultaneous objects
        simPdf = self.get("simPdf").getPdf(chan)
        simRds = self.get("simRds")
        # bkg pdf
        bkgPdfName="bkgPdf_{0}_bkg".format(firstUpper(chan))
        bkgPdf = self.get(bkgPdfName)
        x = self.get("x")
        # make cut version of simRdh
        cutName = "sliced_"+chan+"_rds"
        cutRds = RooDataSet(cutName,cutName,
                            RooArgSet(x),
                            RooFit.Import(simRds),
                            RooFit.Cut("channelName==channelName::{0}".format(chan)),
                            )
        cutName = "sliced_"+chan+"_rdh"
        cutRdh = cutRds.binnedClone(cutName,cutName)
        # cutRdh = self.get("simRdh")
        # make plotables
        dat = rw.rooWrap(cutRdh,x)
        pdf = rw.rooWrap(simPdf,x)
        bkg = rw.rooWrap(bkgPdf,x)
        pdf.cut(minRange=self.get("fullMin").getVal(),maxRange=self.get("fullMax").getVal())
        bkg.cut(minRange=self.get("fullMin").getVal(),maxRange=self.get("fullMax").getVal())
        dat.cut(minRange=self.get("fullMin").getVal(),maxRange=self.get("fullMax").getVal())
        # normalize


        ######################
        # make matplotlib plot
        ######################
        import matplotlib.gridspec as gridspec
        grid = gridspec.GridSpec(3,1,height_ratios=[4,1,1])
        grid.update(wspace=0.025, hspace=0.05)

        ###########
        # top plot
        ###########
        ax = plt.subplot(grid[0])
        # warning about normalization
        try:
            pdf.normalizeTo(dat,minRange=self.get("fullMin").getVal(),maxRange=self.get("fullMax").getVal())
            bkg.normalizeTo(dat,minRange=self.get("fullMin").getVal(),maxRange=self.get("fullMax").getVal())
            # make plot
        except:
            plt.text(0.5, 0.5, 'Error normalizing PDF', horizontalalignment='center',
                 verticalalignment='center', transform=plt.gca().transAxes)
        # toy data warning
        if self._useToy:
            print "Placing toy data warning"
            plt.text(0.5, 0.9, "Toy Data", horizontalalignment='center', color="r",
                 verticalalignment='center', transform=plt.gca().transAxes)
        objs=[]
        datXData = ma.masked_where(dat.y<=0,dat.x)
        datYData = ma.masked_where(dat.y<=0,dat.y)
        pdfYData = pdf.y
        bkgYData = bkg.y
        # blinds
        datYData = self._blindArray(datXData,datYData)
        pdfYData = self._blindArray(datXData,pdfYData)
        bkgYData = self._blindArray(datXData,bkgYData)
        # plt.plot(datXData,datYData,"ko",label="Data")
        objs.append([ax.errorbar(datXData,datYData,yerr=np.sqrt(datYData),fmt="ko",label="Data")])
        # pdf.y=pdf.y*sum(dat.y)/sum(pdf.y)
        objs.append(ax.plot(pdf.x, pdfYData,color="#1f77b4",label="S+B Fit"))
        objs.append(ax.plot(pdf.x, bkgYData,color="#1f77b4",linestyle=":",label="B Fit"))
        for l in labels:
            objs.append(ax.plot([],[]," ",label=l))
        ticksInside(plt,removeXLabel=True)
        plt.title(title,fontsize=12)
        plt.ylabel("Events")
        # plt.legend(fontsize=14,loc=1, frameon=False)
        plt.legend([o[0] for o in objs],[i[0].get_label() for i in objs],fontsize=14,loc=1, frameon=False)

        ratioMargin = 2.5

        #####################
        #### Bottom plot ###
        #####################
        ax = plt.subplot(grid[1],sharex=ax)
        print "plotting bottom:"
        print list(pdf.y)
        print list(dat.y)
        bottomX = datXData
        bottomRatio = pdfYData/datYData
        bottomError = np.sqrt(pdfYData**2 / datYData**3)
        # bottomRatio = pdf.y/dat.y # this is what gets plotted
        # bottomError = np.sqrt(pdf.y**2 / dat.y**3)
        # bottomX     = ma.masked_where(dat.y==0, pdf.x)
        # bottomRatio = ma.masked_where(dat.y==0, bottomRatio)
        # bottomError = ma.masked_where(dat.y==0, bottomError)
        # scale bottom plot
        stdev  = np.std(bottomRatio)
        mean   = np.mean(bottomRatio)
        pltLow = mean-ratioMargin*stdev
        pltHigh= mean+ratioMargin*stdev
        print "==== stdev=", stdev
        print "==== mean=", mean
        print "==== pltLow=", pltLow
        print "==== pltHigh=", pltHigh
        print dat.y
        plt.ylim((pltLow,pltHigh))
        ax.plot([min(pdf.x),max(pdf.x)], [1,1],"k",alpha=0.25)
        ax.errorbar(bottomX,bottomRatio,yerr=bottomError,fmt="ko",label="Ratio")
        # plot arrows for the points above or below the plot
        above = ma.masked_where(bottomRatio<pltHigh, bottomX)
        below = ma.masked_where(bottomRatio>pltLow,  bottomX)
        ax.plot(above, [mean+ratioMargin*0.75*stdev]*len(above),"r^")
        ax.plot(below, [mean-ratioMargin*0.75*stdev]*len(below),"bv")
        ticksInside(plt,removeXLabel=True)
        plt.ylabel(r"$\frac{S+B}{Data}$")

        #####################
        #### Bottom plot ###
        #####################
        ax = plt.subplot(grid[2],sharex=ax)
        print "plotting bottom:"
        print list(bkg.y)
        print list(dat.y)
        bottomX = datXData
        bottomRatio = bkgYData/datYData
        bottomError = np.sqrt(bkgYData**2 / datYData**3)
        # bottomRatio = bkg.y/dat.y # this is what gets plotted
        # bottomError = np.sqrt(bkg.y**2 / dat.y**3)
        # bottomX     = ma.masked_where(dat.y==0, bkg.x)
        # bottomRatio = ma.masked_where(dat.y==0, bottomRatio)
        # bottomError = ma.masked_where(dat.y==0, bottomError)
        # scale bottom plot
        stdev  = np.std(bottomRatio)
        mean   = np.mean(bottomRatio)
        pltLow = mean-ratioMargin*stdev
        pltHigh= mean+ratioMargin*stdev
        print "==== stdev=", stdev
        print "==== mean=", mean
        print "==== pltLow=", pltLow
        print "==== pltHigh=", pltHigh
        print dat.y
        plt.ylim((pltLow,pltHigh))
        ax.plot([min(bkg.x),max(bkg.x)], [1,1],"k",alpha=0.25)
        ax.errorbar(bottomX,bottomRatio,yerr=bottomError,fmt="ko",label="Ratio")
        # plot arrows for the points above or below the plot
        above = ma.masked_where(bottomRatio<pltHigh, bottomX)
        below = ma.masked_where(bottomRatio>pltLow,  bottomX)
        ax.plot(above, [mean+ratioMargin*0.75*stdev]*len(above),"r^")
        ax.plot(below, [mean-ratioMargin*0.75*stdev]*len(below),"bv")
        ticksInside(plt)
        plt.ylabel(r"$\frac{B}{Data}$")
        plt.xlabel(r"$m_{\mu\mu}$ GeV")

        # other plotting things
        # plt.yscale('log') # careful about NEGATIVES!
        plt.savefig("plots/slice-{0}-{1}.png".format(chan,title),dpi=200,bbox_inches='tight')
        print "#"*100

    def _fixVariables(self,fixed=True):
        """ Fix all variables except x, mu, variables with FFF in name 
            FFF = Force Floating in Fit
        """
        print green("Fixing variables: {0}".format(fixed))
        print "#"*50
        allVars=self.w.allVars() # this seperate step is needed
        iterate=allVars.createIterator()
        var=iterate.Next()
        while var!=None:
            if var.GetName() not in ["x","mu"] and "FFF" not in var.GetName():
                print yellow("Fixing", var.GetName())
                var.setConstant(fixed)
            else:
                print green("NOT Fixing", var.GetName())
            var=iterate.Next()

################################################################################
# Procedures
################################################################################

    def _processHists(self):
        """ Process histograms in _hists
            Create RooDataSets, RooDataHists in dictionaires
        """
        # create RooDataSets and RooDataHists
        # these objects store the *names* of the created roo-objs
        self._datasets  = {} # RooDataSets
        self._datahists = {} # RooDataHists
        for chan in self._chans:
            sig = self._hists[chan]["sig"] # dict of histos, key=MC
            dat = self._hists[chan]["dat"] # dict of histos, key=MC
            bkg = self._hists[chan]["bkg"] # dict of histos, key=MC
            # load histograms as RDH
            self._datahists[chan]        = {}
            self._datahists[chan]["sig"] = self._histsToRdh(sig)
            self._datahists[chan]["dat"] = self._histsToRdh(dat)
            self._datahists[chan]["bkg"] = self._histsToRdh(bkg)
            # load datasets as RDS. If using toy hist, will propagate to RDS
            self._datasets[chan]         = {}
            self._datasets[chan]["sig"]  = self._rdhToRds(self._datahists[chan]["sig"])
            self._datasets[chan]["dat"]  = self._rdhToRds(self._datahists[chan]["dat"])
            self._datasets[chan]["bkg"]  = self._rdhToRds(self._datahists[chan]["bkg"])
            # Optional: load hist directly to RDS
            # self._datasets[chan]["sig"]  = self._histsToRds(sig)
            # self._datasets[chan]["dat"]  = self._histsToRds(dat)
            # self._datasets[chan]["bkg"]  = self._histsToRds(bkg)
            if len(dat)!=1: 
                print red(dat)
                raise BaseException(red("Too many background histograms provided for channel {0}".format(chan)))
        # GET YIELDS BASED ON ROODATAHISTS
        # also, add them to workspace
        x = self.get("x")
        for chan in self._chans:

            nSig = 0
            for name in self._sigNames[chan]:
                sig = rw.rooWrap(self.get(name+"_rdh"),x)
                fullYield = sig.sum(minRange=self.get("fullMin").getVal(),maxRange=self.get("fullMax").getVal())
                nSig += fullYield
                self.addFloat(name+"_nSig",fullYield)
            self.addFloat(chan+"_nSig",nSig)
            # YIELDS FOR BACKGROUND
            nBkg = 0
            for name in self._bkgNames[chan]:
                bkg = rw.rooWrap(self.get(name+"_rdh"),x)
                fullYield = bkg.sum(minRange=self.get("fullMin").getVal(),maxRange=self.get("fullMax").getVal())
                nBkg += fullYield
                self.addFloat(name+"_nBkg",fullYield)
            self.addFloat(chan+"_nBkg",nBkg)
            # YIELDS FOR DATA
            nDat = 0
            for name in self._datNames[chan]:
                dat = rw.rooWrap(self.get(name+"_rdh"),x)
                fullYield = dat.sum(minRange=self.get("fullMin").getVal(),maxRange=self.get("fullMax").getVal())
                nDat += fullYield
                self.addFloat(name+"_nDat",fullYield)
            self.addFloat(chan+"_nDat",nDat)


    def _initWksp(self):
        """ Init workspace 
            Defines self.w
        """
        print green("initWksp")
        integratorPrecision=1e-8
        RooAbsPdf.defaultIntegratorConfig().setEpsRel(integratorPrecision)
        RooAbsPdf.defaultIntegratorConfig().setEpsAbs(integratorPrecision)
        Math.MinimizerOptions.SetDefaultPrecision(integratorPrecision) 
        # create workspace
        self.w = RooWorkspace("w","w")
        # create variable
        self.srMin=120
        self.srMax=130
        self.fullMin=110
        self.fullMax=160
        self.addStr("fullMin[{0}]".format(self.fullMin))
        self.addStr("fullMax[{0}]".format(self.fullMax))
        self.addStr("srMin[{0}]".format(self.srMin))
        self.addStr("srMax[{0}]".format(self.srMax))
        self.addStr("mu[1,-10,100]")
        self.addStr("muBkg[1,-100,100]")
        self.addStr("x[{0},{0},{1}]".format(self.fullMin,self.fullMax))
        # create range
        x = self.get("x")
        x.setBins(100)
        x.setRange("fullRange",self.fullMin,self.fullMax)
        x.setRange("srRange",self.srMin,self.srMax)
        # define sets for storing variables
        self.w.defineSet("chanNp","")# this holds nuissance parameters for channels
        self.w.defineSet("np","")    # this holds nuissance parameters for simFit
        self.w.defineSet("poi","mu") # this is defined for reader's clarity
        self.w.defineSet("obs","x")  # this is defined for reader's clarity
        print green("done initWksp")

    def _setupModels(self):
        """ Set up signal and background models for all channels """
        x = self.get("x")
        # make signal pdf
        for chan in self._chans:
            # list to hold S,B PDF's for this channel
            pdfList = RooArgList("pdfList")
            # list to hold coef for those PDF's
            coefList = RooArgList("coefList")

            sigPdfs = [] # needed to avoid rooFit bug with scopes
            ####################################################################################################
            # MAKE SIG PDF
            ####################################################################################################
            for i, name in enumerate(self._sigNames[chan]): # repeat for every SIG for this CHAN
                print yellow("Making signal pdf for",name)
                sigPdfName  = "sigPdf_{0}".format(firstUpper(name))
                sigCoefName = "nSigCoef_{0}".format(firstUpper(name)) # multiply (extend) signal by this
                # signal coefficient
                self.addStr("prod:{0}(mu,{1})".format(sigCoefName,name+"_nSig"))
                # signal model
                self.addStr("CB_mean_sig{0}[125,110,140]".format(firstUpper(name)))
                self.addStr("GF_mean_sig{0}[125,110,140]".format(firstUpper(name))) 
                self.addStr("RooCBShape::sigCB{0}(x, CB_mean_sig{0}, CB_sigma_sig{0}[2,1,4], CB_a_sig{0}[1.75,1,2], CB_n_sig{0}[1.])".format(firstUpper(name)))
                self.addStr("RooGaussian::sigGF{0}(x, GF_mean_sig{0}, GF_sigma_sig{0}[5,1,6])".format(firstUpper(name)))
                # change 
                # self.addStr("SUM:{0}(sigCB{1}, frac_sig{1}[0.3,0, 0.4]* sigGF{1})".format(sigPdfName,firstUpper(name)))
                self.addStr("SUM:{0}(sigCB{1}, frac_sig{1}[0.3,0, 0.95]* sigGF{1})".format(sigPdfName,firstUpper(name)))
                self.get(sigPdfName).fixAddCoefNormalization(RooArgSet(x),True)
                # make extended pdf (gets added in S+B)
                # sigPdfs.append(RooExtendPdf(sigPdfName,sigPdfName,self.get(sigPdfName+"Pdf"),self.get(sigCoefName),"fullRange"))
                sigPdfs.append(self.get(sigPdfName))
                pdfList.add(sigPdfs[-1])
                coefList.add(self.get(sigCoefName))
                #########################################################
                # perform fit for this channel

            ####################################################################################################
            # MAKE BKG PDF
            ####################################################################################################
            name = chan # there must be only one target for the fit
            bkgPdfName="bkgPdf_{0}_bkg".format(firstUpper(name))
            # addPdfName="addPdf_{0}".format(firstUpper(name))
            bkgCoefName = "nBkgCoef_{0}".format(firstUpper(name)) # multiply (extend) background by this
            bkgCoefSingleName = "nBkgCoefSingle{0}".format(firstUpper(name)) # multiply (extend) background by this
            floatingBkgScaleName = "bkgScale{0}FFF".format(firstUpper(name)) # the floating NP
            # bkg coefficient
            self.addStr("prod:{0}({1}[1,0,2],{2})".format(bkgCoefName,floatingBkgScaleName,chan+"_nDat"))
            #########################################################
            # background model. "FFF" means "Force Floating in Fit"
            self.addStr("RooVoigtian::BW_bg{0}(x,mBW{0}[91.2],ZWidth{0}[2.49],sigma{0}[2])".format(firstUpper(name)))
            self.addStr("a2_bgFFF{0}[0,-1,1]".format(firstUpper(name)))
            self.addStr("a3_bgFFF{0}[2,0,25]".format(firstUpper(name)))
            self.addStr("frac_bgFFF{0}[0.1,0,1]".format(firstUpper(name)))
            self.addStr("EXPR::exp_bg{0}('exp(a2_bgFFF{0}*(x/1))*(1./pow((x/1),a3_bgFFF{0}))',x,a2_bgFFF{0},a3_bgFFF{0})".format(firstUpper(name)))
            self.addStr("SUM::{0}(BW_bg{1},frac_bgFFF{1}*exp_bg{1})".format(bkgPdfName,firstUpper(name)))
            self.get(bkgPdfName).fixAddCoefNormalization(RooArgSet(x),True)
            bkgPdf = self.get(bkgPdfName)
            #########################################################
            # bkgPdf = RooExtendPdf(bkgPdfName,bkgPdfName,self.get(bkgPdfName+"Pdf"),self.get(bkgCoefName),"fullRange") # floating bkg norm
            pdfList.add(bkgPdf) # will be added to workspace in RooAddPdf
            coefList.add(self.get(bkgCoefName)) # will be added to workspace in RooAddPdf
            # nuissance parameter is floating background norm
            self.set("np").add(self.get(floatingBkgScaleName))
            #########################################################

            ####################################################################################################
            # MAKE COMBINED PDFS
            ####################################################################################################
            channelModelName="channelModel_{0}".format(firstUpper(chan))
            # make pdf
            addPdf = RooAddPdf(channelModelName,channelModelName, pdfList,coefList)
            addPdf.Print()
            # add, (which also adds S, B PDFs)
            self.add(addPdf)
            # make visualization of this pdf
            # self._vizPdf(addPdf)

            ####################################################################################################
            # MAKE FITS, PLOTS
            ####################################################################################################
            bkgName = self._datNames[chan][0]+"_rdh"
            if self._fitOn: self._fit(bkgPdfName,bkgName,"fullRange")
            if self._plotOn:self._simplePlot("bkgFit-{0}-postFit".format(chan),bkgPdfName,bkgName)
            for i, name in enumerate(self._sigNames[chan]): # repeat for every SIG for this CHAN
                sigName = name + "_rdh"
                if self._fitOn: self._fit(sigPdfName,sigName,"fullRange")
                if self._plotOn:self._simplePlot("sigFit-{0}-postFit".format(name),sigPdfName,sigName)

    def _makeCombined(self):
        """ Make combined dataset """
        print green("Making combined datasets")
        x = self.get("x")
        #############################################################
        # make RooCategory
        categories = RooCategory("channelName","channelName")
        [categories.defineType(chan) for chan in self._chans]
        self.add(categories)
        #############################################################
        # make dataset
        # follow instructions: https://root-forum.cern.ch/t/combining-roodatasets-using-std-map-in-pyroot/16471/19
        dataMap = ROOT.std.map('string, RooDataSet*')()
        dataMap.keepalive = list()
        for chan in self._chans:
            print green("Adding channel",chan,"to dataset")
            rdsName = self._datNames[chan][0]+"_rds"
            rds = self.get(rdsName)
            dataMap.keepalive.append(rds)
            pair=ROOT.std.pair("const string, RooDataSet*")(chan, rds)
            dataMap.insert(dataMap.cbegin(), pair)
            # dataMap.insert(dataMap.cend(), pair)
        simRds = RooDataSet("simRds","simRds",RooArgSet(x),RooFit.Index(categories),RooFit.Import(dataMap))
        simRdh = simRds.binnedClone("simRdh","simRdh")
        self.add(simRds)
        self.add(simRdh)
        #############################################################
        # make simultaneous pdf
        simPdf = RooSimultaneous("simPdf","simPdf",categories)
        for chan in self._chans:
            chanModName="channelModel_{0}".format(firstUpper(chan))
            print "*"*50
            print green("Adding channel model",chanModName,"to simPDF")
            self.get(chanModName).Print()
            print "*"*50
            simPdf.addPdf(self.get(chanModName),chan)
        simPdf.fixAddCoefNormalization(RooArgSet(x),True)

        # simPdf.fixAddCoefRange("fullRange",True) # TODO: this doesn't seem to change much, but I think it should be included
        self.add(simPdf)
        #############################################################
        # make plots for checking combined data set
        for chan in self._chans:
            self._dataSlicePlot(chan,"prefit-{0}".format(chan))

    def _doSimFit(self):
        """ Run the simultaneous NLL fit """
        print green("Doing simultaneous fit")
        #############################################################
        # fix all variables not used in sim-fit
        x = self.get("x")
        if self._fixParamsInSimFit:
            self._fixVariables(fixed=True) # TODO: removed at suggestion of Aaron
        # do fit
        print red("start fitting")
        result = self.get("simPdf").fitTo(self.get("simRdh"),NumCPU(8),Save())
        print "RESULT:",(result==False), result, result.Print()
        print "Fitted mu:", self.get("mu").getVal()
        self.muVals.append(self.get("mu").getVal())
        # get best mu value
        muMinFit=self.get("mu").getVal()
        # self.addFloat("postFittedMuValue",muMinFit)
        # print "Best mu:", muMinFit, self.get("postFittedMuValue").getVal()
        print red("done fitting")

        for chan in self._chans:
            self._dataSlicePlot(chan,"postfit-{0}".format(chan))

    def _doNllScan(self):
        """ Run the simultaneous NLL fit """
        print green("Doing simultaneous fit")
        #############################################################
        # fix all variables not used in sim-fit
        # x = self.get("x")
        if self._fixParamsInSimFit:
            self._fixVariables(fixed=True) # TODO: removed at suggestion of Aaron
        # do nll fit
        print red("*** Creating nll ***")
        nll = self.get("simPdf").createNLL(self.get("simRdh"),RooFit.NumCPU(4)) #121018
        nll.SetTitle("nll"); nll.SetName("nll")
        print green("*** Done creating nll ***")
        self.add(nll)
        if False:
            print green("*** Starting NLL fit ***")
            ROOT.RooMinuit(nll).migrad()
            self.muVals.append(self.get("mu").getVal())
            ROOT.RooMinuit(nll).minos()
            self.muVals.append(self.get("mu").getVal())
            print green("*** Done with NLL fit ***")
            for chan in self._chans:
                self._dataSlicePlot(chan,"postfitNll-{0}".format(chan))


