from __future__ import division
import sys, copy, os
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
sys.path = ["/home/prime/dilepton/pyStap0l8ate/source"] + sys.path
sys.path = ["/home/prime/hmumu/ana-200218/code/copyYanlinFits/source"] + sys.path
from ROOT import *
import hmmFit
import loadMiha
import loadQuickAna
from extras import *

blind=[120,130]
os.popen("rm plots/*")
import time
start=time.time()
# hists = loadMiha.loadMiha()

path="/home/prime/hmumu/ana-200218/pickledHists-workingpoint-050918-threeEta/*.pickle"
# hists = loadQuickAna.loadQuickAna(path,"CSC")
hists = loadQuickAna.loadQuickAna(path,"SCNC")
# hists = {"vbfLoose":hists["vbfLoose"]}
# del hists["4lep"]
# del hists["3lep"]


fit = hmmFit.hmmFit(hists,plotOn=True,useToy=True,fixParamsInSimFit=True)

stop=time.time()
print "Run time",round((stop-start)/60,2),"m"
# if fit.exists("postFittedMuValue"):
    # print yellow("Best fit mu",fit.get("postFittedMuValue").getVal())
print yellow("Mu vals:",fit.muVals)
f = TFile.Open("workspace.root","recreate")
fit.w.Write()
f.Close()
print "DONE "*100
sys.stdout.flush() # flush output before root gets... crashy
quit()
